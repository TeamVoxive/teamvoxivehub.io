# What is Team Voxive?

## The "Team"
Team Voxive is a collective name for some projects by Brandon Giesing. It's basically anything that I want to dip into.

## The Person
I am a teenager in Missouri who loves learning about Technology, Animation, News, Music, and more. [You can find out more about me at my personal site!][1]

[1]: http://brandon.giesing.cf
