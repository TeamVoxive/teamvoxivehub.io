# WARNING! AWESOMENESS AHEAD!
Welcome to the official home of **Team Voxive**, a small side project by [Brandon Giesing][1] to create things that people may enjoy or may not.

Click [About](pages/about.md) to read more about me and the "Team". Also, view the Projects dropdown to visit our three projects: Vox Variety, Vox Remover, and Team Voxive Presents: TechHouse!

I hope you enjoy these projects and if you want to improve this site, you can [fork it on GitHub][2] and send me a pull request!

# Vox Variety
-------------
## [SUPORT US ON PATREON!][3]
<div class="container-fluid"><div class="row"><div class="col-md-4"><h2 id="listen-now">LISTEN NOW</h2><div class="radionomy-player"></div></div><div class="col-md-8"><h2 id="listen-now">SCHEDULE</h2><div class="embed-responsive embed-responsive-16by9"><iframe width=100% height=389px class="embed-responsive-item" src="https://docs.google.com/spreadsheets/d/16OVq7_OHc9qRwF5YjXzKCaOxjEmvLaohnA1lqaak-9E/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe></div></div></div></div>

<script>(function (win, doc, script, source, objectName) { (win.RadionomyPlayerObject = win.RadionomyPlayerObject || []).push(objectName); win[objectName] = win[objectName] || function (k, v) { (win[objectName].parameters = win[objectName].parameters || { src: source, version: '1.1' })[k] = v; }; var js, rjs = doc.getElementsByTagName(script)[0]; js = doc.createElement(script); js.async = 1; js.src = source; rjs.parentNode.insertBefore(js, rjs); }(window, document, 'script', 'https://www.radionomy.com/js/radionomy.player.js', 'radplayer'));
radplayer('url', 'voxvariety');
radplayer('type', 'medium');
radplayer('autoplay', '0');
radplayer('volume', '50');
radplayer('color1', '#000000');
radplayer('color2', '#ffffff');
</script>

[1]: http://brandon.giesing.cf
[2]: https://github.com/TeamVoxive/teamvoxive.github.io/fork
[3]: https://www.patreon.com/voxvariety