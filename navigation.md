<!-- Name of your wiki -- Do NOT remove the leading `#` character.  -->

# Team Voxive

<!-- Default theme -- (Read: http://dynalon.github.io/mdwiki/#!customizing.md#Theme_chooser) -->

[gimmick:theme](journal)

<!-- Navigation -- (Read: http://dynalon.github.io/mdwiki/#!quickstart.md#Adding_a_navigation) -->

[About](pages/about.md)

[Projects]()

  * # Vox Variety
  * [Radionomy Page][v1]
  * [NikNot Page][v2]
  * [TuneIn Page][v3]
  - - - -
  * # Vox Remover
  * [XDA Thread][r1]
  * [GitHub][r2]
  - - - -
  * # TechHouse
  * [Google+][th1]
  * [YouTube][th2]

[v1]: http://radionomy.com/en/radio/voxvariety/index
[v2]: http://niknot.com/stations/vox-variety/
[v3]: http://tunein.com/radio/Vox-Variety-s244928/
[r1]: http://forum.xda-developers.com/android/apps-games/tool-vox-remover-t2870264
[r2]: https://github.com/TeamVoxive/vox_remover
[th1]: https://plus.google.com/116782453922961382111
[th2]: https://www.youtube.com/channel/UCQ1ZMKS4fnIWqXS5ao9TaYA

<!-- Let the user choose a theme -- (Read: http://dynalon.github.io/mdwiki/#!quickstart.md#Adding_a_navigation) -->
<!--
[gimmick:themechooser](Choose theme)
-->
